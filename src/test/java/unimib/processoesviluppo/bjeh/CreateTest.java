package unimib.processoesviluppo.bjeh;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class CreateTest {
    
    private EntityManagerFactory emf;
    private EntityManager em;
    private Giocatore nuovoGiocatore;
    private Carta nuovaCarta;
    private Mano nuovaMano;
    private Partita nuovaPartita;
    private Tavolo nuovoTavolo;
    
    //Inizializzazione della Persistence Unit e dell'Entity manager
    public CreateTest() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
        em =  emf.createEntityManager();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    //Per ogni test nel setUp vengono instanziate delle classi per ricreare un ambiente reale.
    @Before
    public void setUp() {
        nuovoGiocatore = new Giocatore();
        nuovoGiocatore.setNome("Leonardo");
        nuovoGiocatore.setCognome("Bianchi");
        nuovoGiocatore.setEmail("leonardo_bianchi@gmail.com");
        nuovoGiocatore.setNumero_telefono("3349848733");
        nuovaCarta = new Carta();
        nuovaCarta.setColore_retro("Nero");
        nuovaCarta.setNumero("Cinque");
        nuovaCarta.setSeme("Quadri");       
        nuovaMano = new Mano();
        nuovaPartita = new Partita();
        nuovoTavolo = new Tavolo();
        nuovoTavolo.setDealer("GianniDealer");
        nuovoTavolo.setNome_gioco("Black Jack");
    }
    
    //Viceversa nel tearDown vengono cancellate tutte le istanze precedenti
    @After
    public void tearDown() {
        em.getTransaction().begin();
        nuovoGiocatore = em.merge(nuovoGiocatore);
        nuovaCarta = em.merge(nuovaCarta);
        nuovaMano = em.merge(nuovaMano);
        nuovoTavolo = em.merge(nuovoTavolo);
        nuovaPartita = em.merge(nuovaPartita);
        em.remove(nuovoGiocatore);
        em.remove(nuovaCarta);
        em.remove(nuovaMano);
        em.remove(nuovoTavolo);
        em.remove(nuovaPartita);
        em.getTransaction().commit();
        em.close();
    }
    
    //I test per le Create dei dati sono stati svolti aggiungendo una nuova istanza di un oggetto con l'apposito metodo di aggiunta implementato all'interno della classe Create
    // L'istanza viene poi ricercata con il metodo "find" all'interno della tabella corrispondente tramite il suo id e poi memorizzata in un nuovo oggetto
    // Infine viene confrontato l'oggetto trovato con la "find" con l'istanza che era stata aggiunta precedentemente tramite il metodo della Create
    // Se il confronto stabilisce che sono uguali tramite la "assertEquals" il test ha succcesso.
    @Test
    public void testAggiungiCarta() {
        System.out.println("Test AggiungiCarta.");
        Create create = new Create();
        create.aggiungiCarta(nuovaCarta);
        Carta cartaAggiunta = em.find(Carta.class, nuovaCarta.getId());
        assertEquals(nuovaCarta, cartaAggiunta);
    }
    
    @Test
    public void testAggiungiMano() {
        System.out.println("Test AggiungiMano.");
        Create create = new Create();
        create.aggiungiMano(nuovaMano);
        Mano manoAggiunta = em.find(Mano.class, nuovaMano.getId());
        assertEquals(nuovaMano, manoAggiunta);
    }
    
    @Test
    public void testAggiungiGiocatore() {
        System.out.println("Test AggiungiGiocatore.");
        Create create = new Create();
        create.aggiungiGiocatore(nuovoGiocatore);
        Giocatore giocatoreAggiunto = em.find(Giocatore.class, nuovoGiocatore.getId());
        assertEquals(nuovoGiocatore, giocatoreAggiunto);
    }
    
    @Test
    public void testAggiungiPartita() {
        System.out.println("Test AggiungiPartita.");
        Create create = new Create();
        create.aggiungiPartita(nuovaPartita);
        Partita partitaAggiunta = em.find(Partita.class, nuovaPartita.getId());
        assertEquals(nuovaPartita, partitaAggiunta);
    }  
    
    @Test
    public void testAggiungiTavolo() {
        System.out.println("Test AggiungiTavolo.");
        Create create = new Create();
        create.aggiungiTavolo(nuovoTavolo);
        Tavolo tavoloAggiunto = em.find(Tavolo.class, nuovoTavolo.getId());
        assertEquals(nuovoTavolo, tavoloAggiunto);
    }
}
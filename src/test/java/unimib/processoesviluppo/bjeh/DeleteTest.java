package unimib.processoesviluppo.bjeh;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class DeleteTest {
    
    private EntityManagerFactory emf;
    private EntityManager em;
    private Giocatore nuovoGiocatore; 
    private Carta nuovaCarta;
    private Mano nuovaMano; 
    private Partita nuovaPartita;
    private Tavolo nuovoTavolo;
  
    //Inizializzazione della Persistence Unit e dell'Entity manager
    public DeleteTest() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
        em =  emf.createEntityManager();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    //Per ogni test nel setUp vengono instanziate delle classi per ricreare un ambiente reale.
    @Before
    public void setUp() {
        // Creo un'istanza di due giocatori:
        nuovoGiocatore = new Giocatore();
        nuovoGiocatore.setNome("Leonardo");
        nuovoGiocatore.setCognome("Bianchi");
        nuovoGiocatore.setEmail("leonardo_bianchi@gmail.com");
        nuovoGiocatore.setNumero_telefono("3349848733");
        
        nuovaCarta = new Carta();
        nuovaCarta.setColore_retro("Nero");
        nuovaCarta.setNumero("Tre");
        nuovaCarta.setSeme("Cuori");
        
        nuovaMano = new Mano();
        nuovaPartita = new Partita();
      
        nuovoTavolo = new Tavolo();
        nuovoTavolo.setDealer("GianniDealer");
        nuovoTavolo.setNome_gioco("Black Jack");
     
        Create create = new Create();
        create.aggiungiGiocatore(nuovoGiocatore);
        create.aggiungiCarta(nuovaCarta);
        create.aggiungiMano(nuovaMano);
        create.aggiungiTavolo(nuovoTavolo);
        create.aggiungiPartita(nuovaPartita);
    }
    
    //Viceversa nel tearDown vengono cancellate tutte le istanze precedenti
    @After
    public void tearDown() {
        em.getTransaction().begin();
        nuovoGiocatore = em.merge(nuovoGiocatore);
        nuovaCarta = em.merge(nuovaCarta); 
        nuovaMano = em.merge(nuovaMano);
        nuovoTavolo = em.merge(nuovoTavolo);
        nuovaPartita = em.merge(nuovaPartita);
        em.remove(nuovoGiocatore);
        em.remove(nuovaCarta);
        em.remove(nuovaMano);
        em.remove(nuovoTavolo);
        em.remove(nuovaPartita);
        em.getTransaction().commit();
        em.close();
    }
    
    // I test per le Delete dei dati sono stati svolti confrontando che la lista prima della delete contenga l'elemento cancellato e la lista dopo la delete non contenga l'elemento.
    // Tramite la prima "createNamedQuery" viene memorizzata la lista degli elementi prima di utilizzare la delete implementata nella classe Delete
    // Viene poi eliminato l'oggetto da questa lista
    // La seconda "createNamedQuery" memorizza la lista degli elementi ottenuti dopo l'eliminazione dell'oggetto
    // tramite la "assertTrue" si controlla che la lista iniziale contenesse l'oggetto
    // tramite la "assertFalse" si controlla che la lista finale non contenga più l'oggetto
    // Se i confronti effettuati con le assert non falliscono il test ha succcesso.
    @Test
    public void testEliminaGiocatore() {
        System.out.println("Test EliminaGiocatore");
        List<Giocatore> listaIniziale = new ArrayList<>();
        List<Giocatore> listaFinale = new ArrayList<>();
        listaIniziale = em.createNamedQuery("Giocatore.findAll", Giocatore.class).getResultList();
        Delete delete = new Delete();
        delete.eliminaGiocatore(nuovoGiocatore);
        listaFinale = em.createNamedQuery("Giocatore.findAll", Giocatore.class).getResultList();
        assertTrue(listaIniziale.contains(nuovoGiocatore));
        assertFalse(listaFinale.contains(nuovoGiocatore));
    }
    @Test
    public void testEliminaCarta() {
        System.out.println("Test Elimina carta");
        List<Carta> listaIniziale = new ArrayList<>();
        List<Carta> listaFinale = new ArrayList<>();
        listaIniziale = em.createNamedQuery("Carta.findAll", Carta.class).getResultList();
        Delete delete = new Delete();
        delete.eliminaCarta(nuovaCarta);
        listaFinale = em.createNamedQuery("Carta.findAll", Carta.class).getResultList();
        assertTrue(listaIniziale.contains(nuovaCarta));
        assertFalse(listaFinale.contains(nuovaCarta));
    }
    @Test
    public void testEliminaMano() {
        System.out.println("Test Elimina mano");
        List<Mano> listaIniziale = new ArrayList<>();
        List<Mano> listaFinale = new ArrayList<>();
        listaIniziale = em.createNamedQuery("Mano.findAll", Mano.class).getResultList();
        Delete delete = new Delete();
        delete.eliminaMano(nuovaMano);
        listaFinale = em.createNamedQuery("Mano.findAll", Mano.class).getResultList();
        assertTrue(listaIniziale.contains(nuovaMano));
        assertFalse(listaFinale.contains(nuovaMano));
    }
    
    @Test
    public void testEliminaPartita() {
        System.out.println("Test Elimina partita");
        List<Partita> listaIniziale = new ArrayList<>();
        List<Partita> listaFinale = new ArrayList<>();
        listaIniziale = em.createNamedQuery("Partita.findAll", Partita.class).getResultList();
        Delete delete = new Delete();
        delete.eliminaPartita(nuovaPartita);
        listaFinale = em.createNamedQuery("Partita.findAll", Partita.class).getResultList();
        assertTrue(listaIniziale.contains(nuovaPartita));
        assertFalse(listaFinale.contains(nuovaPartita));
    }
    
    @Test
    public void testEliminaTavolo() {
        System.out.println("Test Elimina tavolo");
        List<Tavolo> listaIniziale = new ArrayList<>();
        List<Tavolo> listaFinale = new ArrayList<>();
        listaIniziale = em.createNamedQuery("Tavolo.findAll", Tavolo.class).getResultList();
        Delete delete = new Delete();
        delete.eliminaTavolo(nuovoTavolo);
        listaFinale = em.createNamedQuery("Tavolo.findAll", Tavolo.class).getResultList();
        assertTrue(listaIniziale.contains(nuovoTavolo));
        assertFalse(listaFinale.contains(nuovoTavolo));
    }   
}
package unimib.processoesviluppo.bjeh;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
public class CaseTest {
    
    private EntityManagerFactory emf;
    private EntityManager em;
    private Giocatore nuovoGiocatore;
    private Giocatore nuovoGiocatore2;
    private Carta nuovaCarta;
    private Carta nuovaCarta2;
    private Carta nuovaCarta3;
    private Mano nuovaMano;
    private Mano nuovaMano2;
    private Partita nuovaPartita;
    private Tavolo nuovoTavolo;
    
    //Inizializzazione della Persistence Unit e dell'Entity manager
    public CaseTest() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
        em =  emf.createEntityManager();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    //Per ogni test nel setUp vengono instanziate delle classi per ricreare un ambiente reale.
    @Before
    public void setUp() {
        nuovoGiocatore = new Giocatore();
        nuovoGiocatore.setNome("Leonardo");
        nuovoGiocatore.setCognome("Bianchi");
        nuovoGiocatore.setEmail("leonardo_bianchi@gmail.com");
        nuovoGiocatore.setNumero_telefono("3349848733");
        nuovoGiocatore2 = new Giocatore();
        nuovoGiocatore2.setNome("Mattia");
        nuovoGiocatore2.setCognome("Rossi");
        nuovoGiocatore2.setEmail("mattia_rossi@gmail.com");
        nuovoGiocatore2.setNumero_telefono("3389448733");
        nuovaCarta = new Carta();
        nuovaCarta.setColore_retro("Nero");
        nuovaCarta.setNumero("Cinque");
        nuovaCarta.setSeme("Quadri");
        nuovaCarta2 = new Carta();
        nuovaCarta2.setColore_retro("Rosso");
        nuovaCarta2.setNumero("Tre");
        nuovaCarta2.setSeme("Cuori");
        nuovaCarta3 = new Carta();
        nuovaCarta3.setColore_retro("Nero");
        nuovaCarta3.setNumero("Due");
        nuovaCarta3.setSeme("Quadri");
        nuovaMano = new Mano();
        nuovaMano2 = new Mano();
        nuovaPartita = new Partita();
        nuovoTavolo = new Tavolo();
        nuovoTavolo.setDealer("GianniDealer");
        nuovoTavolo.setNome_gioco("Black Jack");
    }
    
    //Viceversa nel tearDown vengono cancellate tutte le istanze precedenti
    @After
    public void tearDown() {
        em.getTransaction().begin();
        nuovoGiocatore = em.merge(nuovoGiocatore);
        nuovoGiocatore2 = em.merge(nuovoGiocatore2);
        nuovaCarta = em.merge(nuovaCarta);
        nuovaMano = em.merge(nuovaMano);
        nuovoTavolo = em.merge(nuovoTavolo);
        nuovaPartita = em.merge(nuovaPartita);
        em.remove(nuovoGiocatore);
        em.remove(nuovoGiocatore2);
        em.remove(nuovaCarta);
        em.remove(nuovaMano);
        em.remove(nuovoTavolo);
        em.remove(nuovaPartita);
        em.getTransaction().commit();
        em.close();
    }
    
    // Test generico che verifica tutte le funzionalità insieme
    @Test
    public void test() {
        
        // Aggiunte nel database: un nuovo giocatore, una nuova mano, una nuova partita, un nuovo tavolo
        Create create = new Create();
        create.aggiungiGiocatore(nuovoGiocatore);
        create.aggiungiCarta(nuovaCarta);
        create.aggiungiMano(nuovaMano);
        create.aggiungiPartita(nuovaPartita);
        create.aggiungiTavolo(nuovoTavolo);
        // Con la Update è stata aggiunta la partita al tavolo, il giocatore a quel tavolo, la mano al giocatore e la carta alla mano
        Update update = new Update();
        update.aggiungiPartitaAlTavolo(nuovaPartita, nuovoTavolo);
        update.aggiungiGiocatoreAlTavolo(nuovoGiocatore, nuovoTavolo);
        update.aggiungiManoAlGiocatore(nuovaMano, nuovoGiocatore);
        update.aggiungiCartaAllaMano(nuovaCarta, nuovaMano);
        // Dopodichè un altro giocatore si unisce al tavolo e viene così aggiunto 
        create.aggiungiGiocatore(nuovoGiocatore2);
        update.aggiungiGiocatoreAlTavolo(nuovoGiocatore2, nuovoTavolo);
        // Vengono estrapolati i giocatori presenti nel tavolo tramite il metodo della Read
        Read read = new Read();
        //Viene poi aggiunta una nuova mano al nuovo giocatore ed una nuova carta a quella mano
        create.aggiungiMano(nuovaMano2);
        update.aggiungiManoAlGiocatore(nuovaMano2, nuovoGiocatore2);
        create.aggiungiCarta(nuovaCarta2);
        update.aggiungiCartaAllaMano(nuovaCarta2, nuovaMano2);
        // Il primo giocatore pesca una nuova carta
        create.aggiungiCarta(nuovaCarta3);
        update.aggiungiCartaAllaMano(nuovaCarta3, nuovaMano);
        // Il secondo giocatore si dilegua dal tavolo
        Delete delete = new Delete();
        delete.eliminaGiocatore(nuovoGiocatore2);
        
        System.out.println("giocatori: " + read.getAllGiocatori().toString() );
        assertTrue(read.getAllGiocatori().contains(nuovoGiocatore)); 
        assertFalse(read.getAllGiocatori().contains(nuovoGiocatore2));
     
    }
    
}


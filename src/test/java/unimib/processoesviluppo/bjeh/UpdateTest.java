package unimib.processoesviluppo.bjeh;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UpdateTest {
    
    private EntityManagerFactory emf;
    private EntityManager em;
    private Giocatore nuovoGiocatore;
    private Carta nuovaCarta;
    private Mano nuovaMano;
    private Partita nuovaPartita;
    private Tavolo nuovoTavolo;
    
    //Inizializzazione della Persistence Unit e dell'Entity manager
    public UpdateTest() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
        em =  emf.createEntityManager();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    //Per ogni test nel setUp vengono instanziate delle classi per ricreare un ambiente reale.
    @Before
    public void setUp() { 
        nuovoGiocatore = new Giocatore();
        nuovoGiocatore.setNome("Leonardo");
        nuovoGiocatore.setCognome("Bianchi");
        nuovoGiocatore.setEmail("leonardo_bianchi@gmail.com");
        nuovoGiocatore.setNumero_telefono("3349848733");   
        nuovaCarta = new Carta();
        nuovaCarta.setColore_retro("Nero");
        nuovaCarta.setNumero("Cinque");
        nuovaCarta.setSeme("Quadri");
        nuovaMano = new Mano();
        nuovaPartita = new Partita();
        nuovoTavolo = new Tavolo();
        nuovoTavolo.setDealer("GianniDealer");
        nuovoTavolo.setNome_gioco("Black Jack");
      
        // Assumendo che la create sia corretta, da utilizzare come funzione di libreria.
        Create create = new Create();
        create.aggiungiGiocatore(nuovoGiocatore);
        create.aggiungiTavolo(nuovoTavolo);
        create.aggiungiPartita(nuovaPartita);
        create.aggiungiCarta(nuovaCarta);
        create.aggiungiMano(nuovaMano);
    }
    
    //Viceversa nel tearDown vengono cancellate tutte le istanze precedenti
    @After
    public void tearDown() {
        em.getTransaction().begin();
        nuovoGiocatore = em.merge(nuovoGiocatore);
        nuovaCarta = em.merge(nuovaCarta);
        nuovaMano = em.merge(nuovaMano);
        nuovoTavolo = em.merge(nuovoTavolo);
        nuovaPartita = em.merge(nuovaPartita);
        em.remove(nuovoGiocatore);
        em.remove(nuovaCarta);
        em.remove(nuovaMano);
        em.remove(nuovoTavolo);
        em.remove(nuovaPartita);
        em.getTransaction().commit();
        em.close();
    }

    
    // I test per gli update dei dati sono stati svolti facendo una modifica dell'istanza e andando poi a controllare che effettivamente la modifica sia stata effetttuata
    // La modifica dell'istanza viene effettuata tramite l'apposito metodo implementato nella classe Update
    // Le assert controllano che la lista a cui è stato aggiunto/rimosso l'oggetto lo contengano/non lo contengano più 
    @Test
    public void testAggiungiGiocatoreAlTavolo() {
        System.out.println("Test AggiungiGiocatoreAlTavolo.");
        Update update = new Update();
        update.aggiungiGiocatoreAlTavolo(nuovoGiocatore, nuovoTavolo);
        assertTrue(nuovoTavolo.getLista_giocatori().contains(nuovoGiocatore));
    }
    
    @Test
    public void testEliminaGiocatoreDalTavolo() {
        System.out.println("Test EliminaGiocatoreDalTavolo.");
        Update update = new Update();
        update.aggiungiGiocatoreAlTavolo(nuovoGiocatore, nuovoTavolo);
        update.eliminaGiocatoreDalTavolo(nuovoGiocatore, nuovoTavolo);
        assertFalse(nuovoTavolo.getLista_giocatori().contains(nuovoGiocatore));
    }
    
    @Test
    public void testAggiungiPartitaAlTavolo() {
        System.out.println("Test AggiungiPartitaAlTavolo.");
        Update update = new Update();
        update.aggiungiPartitaAlTavolo(nuovaPartita, nuovoTavolo);
        assertTrue(nuovoTavolo.getLista_partite().contains(nuovaPartita));
    }
    
    @Test
    public void testEliminaPartitaDalTavolo() {
        System.out.println("Test EliminaPartitaDalTavolo.");
        Update update = new Update();
        update.aggiungiPartitaAlTavolo(nuovaPartita, nuovoTavolo);
        update.eliminaPartitaDalTavolo(nuovaPartita, nuovoTavolo);
        assertFalse(nuovoTavolo.getLista_partite().contains(nuovaPartita));
    }
    
    @Test
    public void testAggiungiManoAllaPartita() {
        System.out.println("Test AggiungiManoAllaPartita.");
        Update update = new Update();
        update.aggiungiManoAllaPartita(nuovaMano, nuovaPartita);
        assertTrue(nuovaPartita.getLista_mani().contains(nuovaMano));
    }
    
    @Test
    public void testEliminaManoDallaPartita() {
        System.out.println("Test EliminaManoDallaPartita.");
        Update update = new Update();
        update.aggiungiManoAllaPartita(nuovaMano, nuovaPartita);
        update.eliminaManoDallaPartita(nuovaMano, nuovaPartita);
        assertFalse(nuovaPartita.getLista_mani().contains(nuovaMano));
    }
    
    @Test
    public void testAggiungiManoAlGiocatore() {
        System.out.println("Test AggiungiManoAlGiocatore.");
        Update update = new Update();
        update.aggiungiManoAlGiocatore(nuovaMano, nuovoGiocatore);
        assertTrue(nuovoGiocatore.getLista_mani().contains(nuovaMano));
    }
    
    @Test
    public void testEliminaManoDalGiocatore() {
        System.out.println("Test EliminaManoDalGiocatore.");
        Update update = new Update();
        update.aggiungiManoAlGiocatore(nuovaMano, nuovoGiocatore);
        update.eliminaManoAlGiocatore(nuovaMano, nuovoGiocatore);
        assertFalse(nuovoGiocatore.getLista_mani().contains(nuovaMano));
    }
    
    @Test
    public void testAggiungiCartaAllaMano() {
        System.out.println("Test AggiungiCartaAllaMano.");
        Update update = new Update();
        update.aggiungiCartaAllaMano(nuovaCarta, nuovaMano);
        assertTrue(nuovaMano.getLista_carte().contains(nuovaCarta));
    }
    
    @Test
    public void testEliminaCartaDallaMano() {
        System.out.println("Test EliminaCartaDallaMano.");
        Update update = new Update();
        update.aggiungiCartaAllaMano(nuovaCarta, nuovaMano);
        update.eliminaCartaDallaMano(nuovaCarta, nuovaMano);
        assertFalse(nuovaMano.getLista_carte().contains(nuovaCarta));
    }
}
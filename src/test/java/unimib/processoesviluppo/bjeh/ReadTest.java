package unimib.processoesviluppo.bjeh;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class ReadTest {
    
    private EntityManagerFactory emf;
    private EntityManager em;
    private Giocatore nuovoGiocatore;
    private Giocatore nuovoGiocatore2;
    private Carta nuovaCarta;
    private Carta nuovaCarta2;
    private Mano nuovaMano;
    private Mano nuovaMano2;
    private Partita nuovaPartita;
    private Partita nuovaPartita2;
    private Tavolo nuovoTavolo;
    
    //Inizializzazione della Persistence Unit e dell'Entity manager
    public ReadTest() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
        em =  emf.createEntityManager();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    //Per ogni test nel setUp vengono instanziate delle classi per ricreare un ambiente reale.
    @Before
    public void setUp() {
        nuovoGiocatore = new Giocatore();
        nuovoGiocatore.setNome("Leonardo");
        nuovoGiocatore.setCognome("Bianchi");
        nuovoGiocatore.setEmail("leonardo_bianchi@gmail.com");
        nuovoGiocatore.setNumero_telefono("3349848733");
        nuovoGiocatore2 = new Giocatore();
        nuovoGiocatore2.setNome("Mattia");
        nuovoGiocatore2.setCognome("Rossi");
        nuovoGiocatore2.setEmail("mattia_rossi@gmail.com");
        nuovoGiocatore2.setNumero_telefono("3338730303");
 
        nuovaCarta = new Carta();
        nuovaCarta.setColore_retro("Nero");
        nuovaCarta.setNumero("Cinque");
        nuovaCarta.setSeme("Quadri");
        nuovaCarta2 = new Carta();
        nuovaCarta2.setColore_retro("Rosso");
        nuovaCarta2.setNumero("Tre");
        nuovaCarta2.setSeme("Picche");
        
        nuovaMano = new Mano();
        nuovaMano2 = new Mano();
        nuovaPartita = new Partita();     
        nuovaPartita2 = new Partita();

        nuovoTavolo = new Tavolo();
        nuovoTavolo.setDealer("GianniDealer");
        nuovoTavolo.setNome_gioco("Black Jack");
        
        Create create = new Create();
        create.aggiungiGiocatore(nuovoGiocatore);
        create.aggiungiGiocatore(nuovoGiocatore2);
        create.aggiungiCarta(nuovaCarta);
        create.aggiungiCarta(nuovaCarta2);
        create.aggiungiMano(nuovaMano);
        create.aggiungiMano(nuovaMano2);
        create.aggiungiPartita(nuovaPartita);
        create.aggiungiPartita(nuovaPartita2);
        create.aggiungiTavolo(nuovoTavolo);
    }
    
    //Viceversa nel tearDown vengono cancellate tutte le istanze precedenti
    @After
    public void tearDown() {
        em.getTransaction().begin();
        nuovoGiocatore = em.merge(nuovoGiocatore);
        nuovoGiocatore2 = em.merge(nuovoGiocatore2);
        nuovaCarta = em.merge(nuovaCarta);
        nuovaCarta2 = em.merge(nuovaCarta2);
        nuovaMano = em.merge(nuovaMano);
        nuovaMano2 = em.merge(nuovaMano2);
        nuovoTavolo = em.merge(nuovoTavolo);
        nuovaPartita = em.merge(nuovaPartita);
        nuovaPartita2 = em.merge(nuovaPartita2);
        em.remove(nuovoGiocatore);
        em.remove(nuovoGiocatore2);
        em.remove(nuovaCarta);
        em.remove(nuovaCarta2);
        em.remove(nuovaMano);
        em.remove(nuovaMano2);
        em.remove(nuovoTavolo);
        em.remove(nuovaPartita);
        em.remove(nuovaPartita2);
        em.getTransaction().commit();
        em.close();
    }

    // I test per le Read dei dati sono stati svolti leggendo tutti gli elementi presenti in una determinata tabella tramite il metodo apposito implementato nella classe Read e memorizzandoli in un'apposita lista
    // La lettura viene poi ripetuta tramite il metodo "createNamedQuery" ed il risultato viene memorizzato in una seconda lista
    // Infine le due liste vengono confrontate tramite la "assertEquals", se il confronto stabilisce che sono uguali il test ha succcesso.
    public void testLeggiGiocatori() {
        System.out.println("Test leggiGiocatori.");
        Read read = new Read();
        List<Giocatore> nuoviGiocatori = new ArrayList<>();
        nuoviGiocatori = read.getAllGiocatori();
        List<Giocatore> nuoviGiocatori2 = new ArrayList<>();
        nuoviGiocatori2 = em.createNamedQuery("Giocatore.findAll", Giocatore.class).getResultList();
        assertEquals(nuoviGiocatori2, nuoviGiocatori);
    }
    
    @Test
    public void testLeggiCarte() {
        System.out.println("Test leggiCarte.");
        Read read = new Read();
        List<Carta> nuoveCarte = new ArrayList<>();
        nuoveCarte = read.getAllCarte();
        List<Carta> nuoveCarte2 = new ArrayList<>();
        nuoveCarte2 = em.createNamedQuery("Carta.findAll", Carta.class).getResultList();
        assertEquals(nuoveCarte2, nuoveCarte);
    }
    
    @Test
    public void testLeggiMani() {
        System.out.println("Test leggiMani.");
        Read read = new Read();
        List<Mano> nuoveMani = new ArrayList<>();
        nuoveMani = read.getAllMani();
        List<Mano> nuoveMani2 = new ArrayList<>();
        nuoveMani2 = em.createNamedQuery("Mano.findAll", Mano.class).getResultList();
        assertEquals(nuoveMani2, nuoveMani);
    }
    
    @Test
    public void testLeggiPartite() {
        System.out.println("Test leggiPartite.");
        Read read = new Read();
        List<Partita> nuovePartite = new ArrayList<>();
        nuovePartite = read.getAllPartite();
        List<Partita> nuovePartite2 = new ArrayList<>();
        nuovePartite2 = em.createNamedQuery("Partita.findAll", Partita.class).getResultList();
        assertEquals(nuovePartite2, nuovePartite);
    }
    
    @Test
    public void testLeggiTavoli() {
        System.out.println("Test leggiTavoli.");
        Read read = new Read();
        List<Tavolo> nuoviTavoli = new ArrayList<>();
        nuoviTavoli = read.getAllTavoli();
        List<Tavolo> nuoviTavoli2 = new ArrayList<>();
        nuoviTavoli2 = em.createNamedQuery("Tavolo.findAll", Tavolo.class).getResultList();
        assertEquals(nuoviTavoli2, nuoviTavoli);
    }
}
package unimib.processoesviluppo.bjeh;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Read {
    
    // Viene utilizzato l'EntityManager che rende persistenti le entità
    // Persistence Unit indica tutte le classi gestite dall'Entity Manager
    private final EntityManagerFactory emf;
    
    public Read() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
    }
    
    // createNamedQuery consente di selezionare tutti i record presenti nella tabella considerata
    public List<Giocatore> getAllGiocatori() {
       EntityManager em = emf.createEntityManager();
       List<Giocatore> lista_giocatori = em.createNamedQuery("Giocatore.findAll", Giocatore.class).getResultList();
       em.close();
       return lista_giocatori;
    }
    
    public List<Carta> getAllCarte() {
       EntityManager em = emf.createEntityManager();
       List<Carta> lista_carte = em.createNamedQuery("Carta.findAll", Carta.class).getResultList();
       em.close();
       return lista_carte;
    }
    
    public List<Mano> getAllMani() {
       EntityManager em = emf.createEntityManager();
       List<Mano> lista_mani = em.createNamedQuery("Mano.findAll", Mano.class).getResultList();
       em.close();
       return lista_mani;
    }
    
    public List<Partita> getAllPartite() {
       EntityManager em = emf.createEntityManager();
       List<Partita> lista_partite = em.createNamedQuery("Partita.findAll", Partita.class).getResultList();
       em.close();
       return lista_partite;
    }
    
     public List<Tavolo> getAllTavoli() {
        EntityManager em = emf.createEntityManager();
        List<Tavolo> lista_tavoli = em.createNamedQuery("Tavolo.findAll", Tavolo.class).getResultList();
        em.close();
        return lista_tavoli;
    }    
}
package unimib.processoesviluppo.bjeh;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

// Annotazione che consente di definire una query nell'entity.
// Il primo parametro "name" indica il nome della classe, mentre il secondo indica la query
@Entity
@NamedQueries ({
    @NamedQuery(name = "Mano.findAll", query = "SELECT m FROM Mano m")
})

// La classe implementa un Giocatore in un casinò. Ogni istanza, oltre ad avere le informazioni sensibili di una persona, 
// ha un ID autoincrementale e assegnato automaticamete. Inoltre l'istanza viene associata ad un tavolo ed a una lista di mani.
public class Mano implements Serializable {

    private static final long serialVersionUID = 1L;
    // Annotazioni che permettono la creazione automatica della chiave
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    // Le annotazioni ManyToOne e OneToMany indicano le relazioni uno a molti con le altre classi.
    // L'opzione cascade consente la propagazione delle operazioni di aggiornamento e rimozione su oggetti collegati tra loro dalle relazioni.
    // CascadeType.ALL indica che la persistenza propagherà in cascata tutte le operazioni di Entity Manager alle entità correlate
    // JoinTable esplicita le relazioni tra le tabelle ed in particolare specifica la tabella che memorizza i riferimenti alle chiavi primarie con l'attributo "name".
    // joinColumns ed inverseJoinColumns fanno riferimento invece alle chiavi primarie delle due tabelle.
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "GIOCATORE_MANO",
            joinColumns = {@JoinColumn(name = "MANO_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "GIOCATORE_ID", referencedColumnName = "ID")} )
    private Giocatore giocatore;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "PARTITA_MANO",
            joinColumns = {@JoinColumn(name = "MANO_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "PARTITA_ID", referencedColumnName = "ID")} )
    private Partita partita;
    
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "MANO_CARTA",
            joinColumns = {@JoinColumn(name = "MANO_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "CARTA_ID", referencedColumnName = "ID")} )
    private List<Carta> lista_carte;

    public Mano() {
    }
    
    //Sono stati implementati i vari getter e setter per tutti gli attributi
    public void setId(Long id) {
        this.id = id;
    }

    public void setGiocatore(Giocatore giocatore) {
        this.giocatore = giocatore;
    }
    
    public void setPartita(Partita partita) {
        this.partita = partita;
    }

    public void setLista_carte(List<Carta> lista_carte) {
        this.lista_carte = lista_carte;
    }

    public Long getId() {
        return id;
    }
   
    public Giocatore getGiocatore() {
        return giocatore;
    }
    
    public Partita getPartita() {
        return partita;
    }

    public List<Carta> getLista_carte() {
        return lista_carte;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mano)) {
            return false;
        }
        Mano other = (Mano) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "unimib.processoesviluppo.bjeh.Mano[ id=" + id + " ]";
    }  
}
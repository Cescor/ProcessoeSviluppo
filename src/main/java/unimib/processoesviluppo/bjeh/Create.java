package unimib.processoesviluppo.bjeh;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Create {
    
    // Viene utilizzato l'EntityManager che rende persistenti le entità
    // la Persistence Unit indica tutte le classi gestite dall'Entity Manager
    private final EntityManagerFactory emf;
    
    public Create() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
    }
    
    
    // il metodo persist crea un nuovo record nel database per l’entità
    public void aggiungiGiocatore(Giocatore giocatore) {  
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(giocatore);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void aggiungiCarta(Carta carta) {  
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(carta);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void aggiungiMano(Mano mano) {  
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(mano);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void aggiungiPartita(Partita partita) {  
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(partita);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void aggiungiTavolo(Tavolo tavolo) {  
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(tavolo);
        em.getTransaction().commit();
        em.close();  
    }   
}
package unimib.processoesviluppo.bjeh;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

// Annotazione che consente di definire una query nell'entity.
// Il primo parametro "name" indica il nome della classe, mentre il secondo indica la query
@Entity
@NamedQueries ({
    @NamedQuery(name = "Tavolo.findAll", query = "SELECT t FROM Tavolo t")
})

// La classe implementa un tavolo di un casinò. Ogni istanza, oltre ad avere le informazioni necessarie per descrivere un tavolo, 
// ha un ID autoincrementale e assegnato automaticamete. Inoltre l'istanza viene associata ad una lista di giocatori e ad una lista di partite. 
public class Tavolo implements Serializable {

    private static final long serialVersionUID = 1L;
    // Annotazioni che permettono la creazione automatica della chiave
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String dealer;
    private String nome_gioco; 
    
    // Le annotazioni ManyToOne e OneToMany indicano le relazioni uno a molti con le altre classi.
    // L'opzione cascade consente la propagazione delle operazioni di aggiornamento e rimozione su oggetti collegati tra loro dalle relazioni.
    // CascadeType.ALL indica che la persistenza propagherà in cascata tutte le operazioni di Entity Manager alle entità correlate
    // JoinTable esplicita le relazioni tra le tabelle ed in particolare specifica la tabella che memorizza i riferimenti alle chiavi primarie con l'attributo "name".
    // joinColumns ed inverseJoinColumns fanno riferimento invece alle chiavi primarie delle due tabelle.
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "TAVOLO_GIOCATORE",
            joinColumns = {@JoinColumn(name = "TAVOLO_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "GIOCATORE_ID", referencedColumnName = "ID")} )
    private List<Giocatore> lista_giocatori;
    
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "TAVOLO_PARTITA",
            joinColumns = {@JoinColumn(name = "TAVOLO_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "PARTITA_ID", referencedColumnName = "ID")} )
    private List<Partita> lista_partite;
    
// Inizialmente le variabili vengo settate a null    
    public Tavolo() {
        dealer = null;
        nome_gioco = null;
        //lista_giocatori = new ArrayList<>();
        //lista_partite = new ArrayList<>();
    }

    //Sono stati implementati i vari getter e setter per tutti gli attributi
    public Long getId() {
        return id;
    }

    public String getDealer() {
        return dealer;
    }

    public String getNome_gioco() {
        return nome_gioco;
    }
    
    public List<Giocatore> getLista_giocatori() {
        return lista_giocatori;
    }

    public List<Partita> getLista_partite() {
        return lista_partite;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public void setNome_gioco(String nome_gioco) {
        this.nome_gioco = nome_gioco;
    }
    
    public void setLista_giocatori(List<Giocatore> lista_giocatori) {
        this.lista_giocatori = lista_giocatori;
    }

    public void setLista_partite(List<Partita> lista_partite) {
        this.lista_partite = lista_partite;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tavolo)) {
            return false;
        }
        Tavolo other = (Tavolo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "unimib.processoesviluppo.bjeh.Tavolo[ id=" + id + " ]";
    }
}
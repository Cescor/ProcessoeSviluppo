package unimib.processoesviluppo.bjeh;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Update {
    
    // Viene utilizzato l'EntityManager che rende persistenti le entità
    // Persistence Unit indica tutte le classi gestite dall'Entity Manager
    private final EntityManagerFactory emf;
        
    public Update() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
    }
    
    // Si aggiunge un nuovo giocatore ad un tavolo, dopodichè con il metodo merge si aggiorna il database in modo permanente
    // Si procede analogamente per tutte le altre funzioni di aggiunta
    public void aggiungiGiocatoreAlTavolo(Giocatore giocatore, Tavolo tavolo) {  
        EntityManager em = emf.createEntityManager();
        tavolo.getLista_giocatori().add(giocatore);
        em.getTransaction().begin();
        em.merge(tavolo);
        em.getTransaction().commit();
        em.close();  
    }
    
    // Si elimina un giocatore da un tavolo, dopodichè con il metodo merge si aggiorna il database in modo permanente
    // Si procede analogamente per tutte le altre funzioni di eliminazione
    public void eliminaGiocatoreDalTavolo(Giocatore giocatore, Tavolo tavolo) {  
        EntityManager em = emf.createEntityManager();
        tavolo.getLista_giocatori().remove(giocatore);
        em.getTransaction().begin();
        em.merge(tavolo);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void aggiungiPartitaAlTavolo(Partita partita, Tavolo tavolo) {  
        EntityManager em = emf.createEntityManager();
        tavolo.getLista_partite().add(partita);
        em.getTransaction().begin();
        em.merge(tavolo);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void eliminaPartitaDalTavolo(Partita partita, Tavolo tavolo) {  
        EntityManager em = emf.createEntityManager();
        tavolo.getLista_partite().remove(partita);
        em.getTransaction().begin();
        em.merge(tavolo);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void aggiungiManoAllaPartita(Mano mano,Partita partita) {  
        EntityManager em = emf.createEntityManager();
        partita.getLista_mani().add(mano);
        em.getTransaction().begin();
        em.merge(partita);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void eliminaManoDallaPartita(Mano mano,Partita partita) {  
        EntityManager em = emf.createEntityManager();
        partita.getLista_mani().remove(mano);
        em.getTransaction().begin();
        em.merge(partita);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void aggiungiManoAlGiocatore(Mano mano, Giocatore giocatore) {  
        EntityManager em = emf.createEntityManager();
        giocatore.getLista_mani().add(mano);
        em.getTransaction().begin();
        em.merge(giocatore);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void eliminaManoAlGiocatore(Mano mano, Giocatore giocatore) {  
        EntityManager em = emf.createEntityManager();
        giocatore.getLista_mani().remove(mano);
        em.getTransaction().begin();
        em.merge(giocatore);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void aggiungiCartaAllaMano(Carta carta, Mano mano) {
        EntityManager em = emf.createEntityManager();
        mano.getLista_carte().add(carta);
        em.getTransaction().begin();
        em.merge(mano);
        em.getTransaction().commit();
        em.close();
    }
    
    public void eliminaCartaDallaMano(Carta carta, Mano mano) {
        EntityManager em = emf.createEntityManager();
        mano.getLista_carte().remove(carta);
        em.getTransaction().begin();
        em.merge(mano);
        em.getTransaction().commit();
        em.close();
    }
}
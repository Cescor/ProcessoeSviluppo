package unimib.processoesviluppo.bjeh;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

// Annotazione che consente di definire una query nell'entity.
// Il primo parametro "name" indica il nome della classe, mentre il secondo indica la query
@Entity
@NamedQueries ({
    @NamedQuery(name = "Carta.findAll", query = "SELECT c FROM Carta c")
})

// La classe implementa una carta in un casinò. Ogni istanza, oltre ad avere le informazioni di una carta, 
// ha un ID autoincrementale e assegnato automaticamete. Inoltre l'istanza viene associata ad una mano.
public class Carta implements Serializable {
    private static final long serialVersionUID = 1L;
    // Annotazioni che permettono la creazione automatica della chiave 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String seme;
    private String numero;
    private String colore_retro;
    
    // Le annotazioni ManyToOne e OneToMany indicano le relazioni uno a molti con le altre classi.
    // L'opzione cascade consente la propagazione delle operazioni di aggiornamento e rimozione su oggetti collegati tra loro dalle relazioni.
    // CascadeType.ALL indica che la persistenza propagherà in cascata tutte le operazioni di Entity Manager alle entità correlate
    // JoinTable esplicita le relazioni tra le tabelle ed in particolare specifica la tabella che memorizza i riferimenti alle chiavi primarie con l'attributo "name".
    // joinColumns ed inverseJoinColumns fanno riferimento invece alle chiavi primarie delle due tabelle.
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "MANO_CARTA",
            joinColumns = {@JoinColumn(name = "CARTA_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "MANO_ID", referencedColumnName = "ID")} )
    private Mano mano;
    
    //Inizializzo la carta con valori nulli
    public Carta() {
        seme = null;
        numero = null;
        colore_retro = null;
    }

    //Sono stati implementati i vari getter e setter per tutti gli attributi
    public void setId(Long id) {
        this.id = id;
    }

    public void setSeme(String seme) {
        this.seme = seme;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public void setColore_retro(String colore_retro) {
        this.colore_retro = colore_retro;
    }
    
    public void setMano(Mano mano) {
        this.mano = mano;
    }

    public Long getId() {
        return id;
    }

    public String getSeme() {
        return seme;
    }

    public String getNumero() {
        return numero;
    }

    public String getColore_retro() {
        return colore_retro;
    }
       
    public Mano getMano() {
        return mano;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Carta)) {
            return false;
        }
        Carta other = (Carta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "unimib.processoesviluppo.bjeh.Carta[ id=" + id + " ]";
    } 
}
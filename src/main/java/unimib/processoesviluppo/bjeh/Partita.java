package unimib.processoesviluppo.bjeh;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

// Annotazione che consente di definire una query nell'entity.
// Il primo parametro "name" indica il nome della classe, mentre il secondo indica la query
@Entity
@NamedQueries ({
    @NamedQuery(name = "Partita.findAll", query = "SELECT p FROM Partita p")
})

// La classe implementa un partita di un casinò. Ogni istanza, oltre ad avere le informazioni necessarie per descrivere una partita, 
// ha un ID autoincrementale e assegnato automaticamete. Inoltre l'istanza viene associata ad una lista di tavoli e ad una lista di mani.
public class Partita implements Serializable {

    private static final long serialVersionUID = 1L;
    // Annotazioni che permettono la creazione automatica della chiave
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    // Le annotazioni ManyToOne e OneToMany indicano le relazioni uno a molti con le altre classi.
    // L'opzione cascade consente la propagazione delle operazioni di aggiornamento e rimozione su oggetti collegati tra loro dalle relazioni.
    // CascadeType.ALL indica che la persistenza propagherà in cascata tutte le operazioni di Entity Manager alle entità correlate
    // JoinTable esplicita le relazioni tra le tabelle ed in particolare specifica la tabella che memorizza i riferimenti alle chiavi primarie con l'attributo "name".
    // joinColumns ed inverseJoinColumns fanno riferimento invece alle chiavi primarie delle due tabelle.
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "TAVOLO_PARTITA",
            joinColumns = {@JoinColumn(name = "PARTITA_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "TAVOLO_ID", referencedColumnName = "ID")} )
    private Tavolo tavolo;
    
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "PARTITA_MANO",
            joinColumns = {@JoinColumn(name = "PARTITA_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "MANO_ID", referencedColumnName = "ID")} )
    private List<Mano> lista_mani;
    
    public Partita() {
        //tavolo = new Tavolo();
        //lista_mani = new ArrayList<>();
    }

    //Sono stati implementati i vari getter e setter per tutti gli attributi
    public void setId(Long id) {
        this.id = id;
    }
   
    public void setTavolo(Tavolo tavolo) {
        this.tavolo = tavolo;
    }

    public void setLista_mani(List<Mano> lista_mani) {
        this.lista_mani = lista_mani;
    }

    public Long getId() {
        return id;
    }
    
    public Tavolo getTavolo() {
        return tavolo;
    }

    public List<Mano> getLista_mani() {
        return lista_mani;
    }
  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partita)) {
            return false;
        }
        Partita other = (Partita) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "unimib.processoesviluppo.bjeh.Partita[ id=" + id + " ]";
    } 
}

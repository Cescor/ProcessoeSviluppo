package unimib.processoesviluppo.bjeh;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Delete {
    
    // Viene utilizzato l'EntityManager che rende persistenti le entità
    // la Persistence Unit indica tutte le classi gestite dall'Entity Manager
    private final EntityManagerFactory emf;
    
    public Delete() {
        emf = Persistence.createEntityManagerFactory("PersistenceUnit");
    }
    
    // Il metodo remove consente di cancellare i dati associati all’entità del database
    // Prima di rimuovere il record nella tabella del database si controlla se è presente nella tabella
    // Se il record non è presente, si aggiorna la tabella aggiungendolo e di seguito rimuovendolo
    // Se il record è presente viene semplicemente rimosso
    public void eliminaGiocatore(Giocatore giocatore) {  
        EntityManager em = emf.createEntityManager();
        Giocatore g = giocatore;
        em.getTransaction().begin();
        if(!em.contains(giocatore))
            g = em.merge(giocatore);
        em.remove(g);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void eliminaCarta(Carta carta) {  
        EntityManager em = emf.createEntityManager();
        Carta c = carta;
        em.getTransaction().begin();
        if(!em.contains(carta))
            c = em.merge(carta);
        em.remove(c);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void eliminaMano(Mano mano) {  
        EntityManager em = emf.createEntityManager();
        Mano m = mano;
        em.getTransaction().begin();
        if(!em.contains(mano))
            m = em.merge(mano);
        em.remove(m);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void eliminaPartita(Partita partita) {  
        EntityManager em = emf.createEntityManager();
        Partita p = partita;
        em.getTransaction().begin();
        if(!em.contains(partita))
            p = em.merge(partita);
        em.remove(p);
        em.getTransaction().commit();
        em.close();  
    }
    
    public void eliminaTavolo(Tavolo tavolo) {  
        EntityManager em = emf.createEntityManager();
        Tavolo t = tavolo;
        em.getTransaction().begin();
        if(!em.contains(tavolo))
            t = em.merge(tavolo);
        em.remove(t);
        em.getTransaction().commit();
        em.close();  
    } 
}
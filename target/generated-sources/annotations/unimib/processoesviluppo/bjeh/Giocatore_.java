package unimib.processoesviluppo.bjeh;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import unimib.processoesviluppo.bjeh.Mano;
import unimib.processoesviluppo.bjeh.Tavolo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-19T12:27:19")
@StaticMetamodel(Giocatore.class)
public class Giocatore_ { 

    public static volatile ListAttribute<Giocatore, Mano> lista_mani;
    public static volatile SingularAttribute<Giocatore, String> cognome;
    public static volatile SingularAttribute<Giocatore, String> numero_telefono;
    public static volatile SingularAttribute<Giocatore, Tavolo> tavolo;
    public static volatile SingularAttribute<Giocatore, String> nome;
    public static volatile SingularAttribute<Giocatore, Long> id;
    public static volatile SingularAttribute<Giocatore, String> email;

}
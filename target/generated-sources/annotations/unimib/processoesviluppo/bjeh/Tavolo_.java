package unimib.processoesviluppo.bjeh;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import unimib.processoesviluppo.bjeh.Giocatore;
import unimib.processoesviluppo.bjeh.Partita;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-19T12:27:19")
@StaticMetamodel(Tavolo.class)
public class Tavolo_ { 

    public static volatile ListAttribute<Tavolo, Giocatore> lista_giocatori;
    public static volatile SingularAttribute<Tavolo, String> dealer;
    public static volatile SingularAttribute<Tavolo, String> nome_gioco;
    public static volatile ListAttribute<Tavolo, Partita> lista_partite;
    public static volatile SingularAttribute<Tavolo, Long> id;

}
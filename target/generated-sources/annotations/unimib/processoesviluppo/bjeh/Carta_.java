package unimib.processoesviluppo.bjeh;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import unimib.processoesviluppo.bjeh.Mano;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-19T12:27:19")
@StaticMetamodel(Carta.class)
public class Carta_ { 

    public static volatile SingularAttribute<Carta, String> colore_retro;
    public static volatile SingularAttribute<Carta, String> numero;
    public static volatile SingularAttribute<Carta, Mano> mano;
    public static volatile SingularAttribute<Carta, Long> id;
    public static volatile SingularAttribute<Carta, String> seme;

}
package unimib.processoesviluppo.bjeh;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import unimib.processoesviluppo.bjeh.Carta;
import unimib.processoesviluppo.bjeh.Giocatore;
import unimib.processoesviluppo.bjeh.Partita;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-19T12:27:19")
@StaticMetamodel(Mano.class)
public class Mano_ { 

    public static volatile ListAttribute<Mano, Carta> lista_carte;
    public static volatile SingularAttribute<Mano, Long> id;
    public static volatile SingularAttribute<Mano, Giocatore> giocatore;
    public static volatile SingularAttribute<Mano, Partita> partita;

}
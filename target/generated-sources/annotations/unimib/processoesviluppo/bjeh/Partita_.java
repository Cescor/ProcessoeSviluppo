package unimib.processoesviluppo.bjeh;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import unimib.processoesviluppo.bjeh.Mano;
import unimib.processoesviluppo.bjeh.Tavolo;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-19T12:27:19")
@StaticMetamodel(Partita.class)
public class Partita_ { 

    public static volatile ListAttribute<Partita, Mano> lista_mani;
    public static volatile SingularAttribute<Partita, Tavolo> tavolo;
    public static volatile SingularAttribute<Partita, Long> id;

}